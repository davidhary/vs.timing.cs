# Timing  Project

A library of timing classes.

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:

```
git clone git@bitbucket.org:davidhary/vs.timing.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\VS\Controls\Timing
```

## Testing

The project includes a few unit test classes. Test applications are under the *Apps* solution folder. 

## Deployment

Deployment projects have not been created for this project.

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](https://www.IntegratedScientificResources.com)
* [Fast timer](https://www.codeproject.com/KB/miscctrl/lescsmultimediatimer.aspx) -- fast timer

## License

This repository is licensed under the [MIT License](https://www.bitbucket.org/davidhary/vs.timing/src/master/LICENSE.md)

## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow](https://www.stackoveflow.com)
* [Fast timer](https://www.codeproject.com/KB/miscctrl/lescsmultimediatimer.aspx) -- fast timer

## Revision Changes

* Version 1.2.4504	05/01/12	Adds x86 project.
* Version 1.2.4498	04/17/12	Implements code analysis rules for .NET 4.0. 

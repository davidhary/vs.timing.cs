## ISR Timing CS<sub>&trade;</sub>: Timing CS Class Library 
### Revision History

*1.2.4504 05/01/12*  
Adds x86 project.

*1.2.4498 04/17/12*  
Implements code analysis rules for .NET 4.0.

*1.2.4365 12/14/11*  
Upgrades to VS2010.

*1.2.4232 08/03/11*  
Standardize code elements and documentation.

*1.2.4213 07/15/11*  
Simplifies the assembly information.

*1.2.2961 02/09/08*  
Is updated to .NET 3.5. Matches ISR Timing CS Library
revision.

*1.0.2915 12/25/07*  
Adds Fast Timer class (not a component).

*1.0.2909 12/19/07*  
Is created.

\(C\) 2007 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Timing Project](https://bitbucket.org/davidhary/vs.timing)  
[Fast Timer](http://www.codeproject.com/KB/miscctrl/lescsmultimediatimer.aspx)

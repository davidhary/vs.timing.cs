﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany("Integrated Scientific Resources")]
[assembly: AssemblyCopyright("(c) 2007 Integrated Scientific Resources, Inc. All rights reserved.")]
[assembly: AssemblyTrademark("Licensed under The MIT License.")]

[assembly: AssemblyCulture("")]
[assembly: System.Resources.NeutralResourcesLanguage("en-US", System.Resources.UltimateResourceFallbackLocation.MainAssembly)]

// Version information for an assembly consists of the following four values:

//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.2.*")]

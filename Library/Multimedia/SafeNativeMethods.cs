﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Diagnostics.CodeAnalysis;

[module: SuppressMessage("Microsoft.Portability", "CA1901:PInvokeDeclarationsShouldBePortable", Scope = "member", Target = "isr.Timing.CS.Multimedia.SafeNativeMethods.#timeSetEvent(System.Int32,System.Int32,isr.Timing.CS.Multimedia.SafeNativeMethods+TimeProc,System.Int32,System.Int32)", MessageId = "3")]
    
namespace isr.Timing.CS.Multimedia
{
    internal static class SafeNativeMethods
    {

       //TO_DO: Use entry point syntax for imports per F.X. Cop.

       // Represents the method that is called by Windows when a timer event occurs.
       internal delegate void TimeProc(int id, int msg, int user, int param1, int param2);

       #region Win32 Multimedia Timer Functions

       // Gets timer capabilities.
       [DllImport("winmm.dll")]
       internal static extern int timeGetDevCaps(ref TimerCaps caps, int sizeOfTimerCaps);

       // Creates and starts the timer.
       [DllImport("winmm.dll")]
       internal static extern int timeSetEvent(int delay, int resolution, TimeProc proc, int user, int mode);

       // Stops and destroys the timer.
       [DllImport("winmm.dll")]
       internal static extern int timeKillEvent(int id);

       // Indicates that the operation was successful.
       internal const int TIMERR_NOERROR = 0;

       #endregion


    }
}

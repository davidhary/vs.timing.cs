﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Diagnostics.CodeAnalysis;

[module: SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Scope = "member", Target = "isr.Timing.CS.Multimedia.FastTimer.#.cctor()", MessageId = "isr.Timing.CS.Multimedia.SafeNativeMethods.timeGetDevCaps(isr.Timing.CS.Multimedia.TimerCaps@,System.Int32)")]
[module: SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Scope = "member", Target = "isr.Timing.CS.Multimedia.FastTimer.#Finalize()", MessageId = "isr.Timing.CS.Multimedia.SafeNativeMethods.timeKillEvent(System.Int32)")]
namespace isr.Timing.CS.Multimedia
{
    /// <summary>
    /// Timer class based on the Multimedia Timer.  Timer events are asynchronous if
    /// a <see cref="ISynchronizeInvoke">synchronizing object</see> is provided.
    /// </summary>
    public sealed class FastTimer : IDisposable
    {

    #region Fast Timer Members

    #region Delegates

        // Represents methods that raise events.
        private delegate void EventRaiser(EventArgs e);

    #endregion

    #region Fields

        // Timer identifier.
        private int _timerID;

        // Timer mode.
        private volatile TimerMode _mode;

        // Period between timer events in milliseconds.
        private volatile int _period;

        // Timer resolution in milliseconds.
        private volatile int _resolution;

        // Called by Windows when a timer periodic event occurs.
        private SafeNativeMethods.TimeProc _timeProc;

        // Represents the method that raises the Tick event.
        private EventRaiser _tickRaiser;

        // Indicates whether or not the timer is running.
        private bool _running;

        // Indicates whether or not the timer is stopping.
        private bool _stopping;

        // Indicates whether or not the timer has been disposed.
        private volatile bool _disposed;

        // Multimedia timer capabilities.
        private static TimerCaps _capabilities;

        // The I SynchronizeInvoke object to use for marshaling events.
        private volatile ISynchronizeInvoke _synchronizingObject;

    #endregion

    #region Events

        /// <summary>
        /// Occurs when the Timer has disposed;
        /// </summary>
        public event System.EventHandler Disposed;

        /// <summary>
        /// Occurs when the Timer has started;
        /// </summary>
        public event EventHandler Started;

        /// <summary>
        /// Occurs when the Timer has stopped;
        /// </summary>
        public event EventHandler Stopped;

        /// <summary>
        /// Occurs when the time period has elapsed.
        /// </summary>
        public event EventHandler Tick;

    #endregion

    #region Construction

        /// <summary>
        /// Initialize class.
        /// </summary>
        static FastTimer()
        {
            // Get multimedia timer capabilities.
            SafeNativeMethods.timeGetDevCaps(ref _capabilities, Marshal.SizeOf(_capabilities));
        }

        /// <summary>
        /// Initializes a new instance of the Fast Timer class.
        /// </summary>
        public FastTimer()
        {
            Initialize();
        }

        ~FastTimer()
        {
            if (_running)
            {
                // Stop and destroy timer.
                // timeKillEvent(_timerID);
                SafeNativeMethods.timeKillEvent(_timerID);
            }
        }

        // Initialize Fast Timer with default values.
        private void Initialize()
        {
            this._mode = TimerMode.Periodic;
            this._period = Capabilities.PeriodMin;
            this._resolution = 1;
            this._tickRaiser = new EventRaiser(OnTick);
        }

    #endregion

    #region Methods

        /// <summary>
        /// Starts the timer.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// The timer has already been disposed.
        /// </exception>
        /// <exception cref="TimerStartException">
        /// The timer failed to start.
        /// </exception>
        public void Start()
        {
            #region Require

            if (_disposed)
            {
                throw new ObjectDisposedException("FastTimer");
            }

            #endregion

            #region Guard

            if (_running)
            {
                return;
            }

            #endregion

            // If the periodic event callback should be used.
            if (Mode == TimerMode.Periodic)
            {

                if (_synchronizingObject == null)
                {
                    _timeProc = new SafeNativeMethods.TimeProc(periodicEventCallback);
                }
                else
                {
                    _timeProc = new SafeNativeMethods.TimeProc(syncPeriodicEventCallback);
                }

            }
            // Else the one shot event callback should be used.
            else
            {

                if (_synchronizingObject == null)
                {
                    _timeProc = new SafeNativeMethods.TimeProc(oneShotEventCallback);
                }
                else
                {
                    _timeProc = new SafeNativeMethods.TimeProc(syncOneShotEventCallback);
                }
           
            }
            // Get timer handle.
            _timerID = SafeNativeMethods.timeSetEvent(Period, Resolution, _timeProc, 0, (int)Mode);

            // If the timer was created successfully.
            if (_timerID != 0)
            {
                _running = true;

                if (_synchronizingObject != null)
                {
                    _synchronizingObject.BeginInvoke(
                        new EventRaiser(OnStarted),
                        new object[] { EventArgs.Empty });
                }
                else
                {
                    OnStarted(EventArgs.Empty);
                }
            }
            else
            {
                throw new TimerStartException("Unable to start multimedia Fast Timer.");
            }
        }

        /// <summary>
        /// Stops timer.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// If the timer has already been disposed.
        /// </exception>
        public void Stop()
        {

            // indicate that the timer as being stopped.
            _stopping = true;

            #region Require

            if (_disposed)
            {
                throw new ObjectDisposedException("FastTimer");
            }

            #endregion

            #region Guard

            if (!_running)
            {
                return;
            }

            #endregion

            // Stop and destroy timer.
            int result = SafeNativeMethods.timeKillEvent(_timerID);

            Debug.Assert(result == SafeNativeMethods.TIMERR_NOERROR);

            // indicate that the timer is not running.
            _running = false;
            _stopping = false;

            if (_synchronizingObject != null)
            {
                _synchronizingObject.BeginInvoke(
                    new EventRaiser(OnStopped),
                    new object[] { EventArgs.Empty });
            }
            else
            {
                OnStopped(EventArgs.Empty);
            }
        }        

        #region Callbacks

        /// <summary>
        /// Periodically invokes the tick event.  Assumes no synchronizer present.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <param name="user"></param>
        /// <param name="param1"></param>
        /// <param name="param2"></param>
        /// <remarks>Called by the Win32 multimedia timer when a timer periodic event occurs.</remarks>
        private void periodicEventCallback(int id, int msg, int user, int param1, int param2)
        {
            OnTick(EventArgs.Empty);
        }
 
        /// <summary>
        /// Periodically invokes the tick event.  
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <param name="user"></param>
        /// <param name="param1"></param>
        /// <param name="param2"></param>
        /// <remarks>Called by the Win32 multimedia timer when a timer periodic event occurs.</remarks>
        private void syncPeriodicEventCallback(int id, int msg, int user, int param1, int param2)
        {
            _synchronizingObject.BeginInvoke(_tickRaiser, new object[] { EventArgs.Empty });
        }

        /// <summary>
        /// Invokes the tick event once. Assumes no synchronizer present.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <param name="user"></param>
        /// <param name="param1"></param>
        /// <param name="param2"></param>
        /// <remarks>Called by the Win32 multimedia timer when a timer one shot event occurs.</remarks>
        private void oneShotEventCallback(int id, int msg, int user, int param1, int param2)
        {
           OnTick(EventArgs.Empty);
           Stop();
        }

        /// <summary>
        /// invokes the tick event once.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msg"></param>
        /// <param name="user"></param>
        /// <param name="param1"></param>
        /// <param name="param2"></param>
        /// <remarks>Called by the Win32 multimedia timer when a timer one shot event occurs.</remarks>
        private void syncOneShotEventCallback(int id, int msg, int user, int param1, int param2)
        {
            _synchronizingObject.BeginInvoke(_tickRaiser, new object[] { EventArgs.Empty });
            Stop();
        }

    #endregion

    #region Event Raiser Methods

        // Raises the Disposed event.
        private void OnDisposed(EventArgs e)
        {
            EventHandler handler = Disposed;

            if(handler != null)
            {
                handler(this, e);
            }
        }

        // Raises the Started event.
        private void OnStarted(EventArgs e)
        {
            EventHandler handler = Started;

            if(handler != null)
            {
                handler(this, e);
            }
        }

        // Raises the Stopped event.
        private void OnStopped(EventArgs e)
        {
            EventHandler handler = Stopped;

            if(handler != null)
            {
                handler(this, e);
            }
        }

        // Raises the Tick event.
        private void OnTick(EventArgs e)
        {
            EventHandler handler = Tick;

            if(handler != null)
            {
                handler(this, e);
            }
        }

    #endregion        

    #endregion

    #region Properties

        /// <summary>
        /// Gets or sets the object used to marshal event-handler calls.
        /// </summary>
        public ISynchronizeInvoke SynchronizingObject
        {
            get
            {
                #region Require

                    if (_disposed)
                    {
                        throw new ObjectDisposedException("FastTimer");
                    }

                #endregion

                return _synchronizingObject;
            }
            set
            {
                #region Require

                    if (_disposed)
                    {
                        throw new ObjectDisposedException("FastTimer");
                    }

                #endregion

                _synchronizingObject = value;
            }
        }

        /// <summary>
        /// Gets or sets the time between Tick events.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// If the timer has already been disposed.
        /// </exception>   
        public int Period
        {
            get
            {
                #region Require

                    if(_disposed)
                    {
                        throw new ObjectDisposedException("FastTimer");
                    }

                #endregion

                return _period;
            }
            set
            {
                #region Require

                    if(_disposed)
                    {
                        throw new ObjectDisposedException("FastTimer");
                    }
                    else if(value < Capabilities.PeriodMin || value > Capabilities.PeriodMax)
                    {
                        throw new ArgumentOutOfRangeException("value", value,
                            "Multimedia Fast Timer period out of range.");
                    }

                #endregion

                _period = value;

                if(_running)
                {
                    Stop();
                    Start();
                }
            }
        }

        /// <summary>
        /// Gets or sets the timer resolution.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// If the timer has already been disposed.
        /// </exception>        
        /// <remarks>
        /// The resolution is in milliseconds. The resolution increases 
        /// with smaller values; a resolution of 0 indicates periodic events 
        /// should occur with the greatest possible accuracy. To reduce system 
        /// overhead, however, you should use the maximum value appropriate 
        /// for your application.
        /// </remarks>
        public int Resolution
        {
            get
            {
                #region Require

                    if(_disposed)
                    {
                        throw new ObjectDisposedException("FastTimer");
                    }

                #endregion

                return _resolution;
            }
            set
            {
                #region Require

                    if(_disposed)
                    {
                        throw new ObjectDisposedException("FastTimer");
                    }
                    else if(value < 0)
                    {
                        throw new ArgumentOutOfRangeException("value", value,
                            "Multimedia Fast Timer resolution out of range.");
                    }

                #endregion

                _resolution = value;

                if (_running)
                {
                    Stop();
                    Start();
                }
            }
        }

        /// <summary>
        /// Gets the timer mode.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// If the timer has already been disposed.
        /// </exception>
        public TimerMode Mode
        {
            get
            {
                #region Require

                    if(_disposed)
                    {
                        throw new ObjectDisposedException("FastTimer");
                    }

                #endregion

                return _mode;
            }
            set
            {
                #region Require

                    if(_disposed)
                    {
                        throw new ObjectDisposedException("FastTimer");
                    }

                #endregion
                
                _mode = value;

                if (_running)
                {
                    Stop();
                    Start();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Timer is running.
        /// </summary>
        public bool Running
        {
            get
            {
                return _running;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Timer is stopping.
        /// This is turned off once the timer is no longer running.
        /// </summary>
        public bool Stopping
        {
            get
            {
                return _stopping;
            }
        }


        /// <summary>
        /// Gets the timer capabilities.
        /// </summary>
        public static TimerCaps Capabilities
        {
            get
            {
                return _capabilities;
            }
        }

    #endregion

    #endregion

    #region IDisposable Members

        /// <summary>
        /// Frees timer resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            #region Guard

            if (_disposed)
            {
                return;
            }

            #endregion

            if (disposing)
            {

                if (_running)
                {
                    Stop();
                }

                _disposed = true;

                OnDisposed(EventArgs.Empty);

                // terminate reference to the synchronizing object to prevent calling an
                // object that might be closing.
               // _synchronizingObject = null;

            }
        }

    #endregion       

    }
}

#region License

/* Copyright (c) 2006 Leslie Sanford
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sub license, and/or 
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software. 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE.
 */

#endregion

#region Contact

/*
 * Leslie Sanford
 * Email: JABBERDABBER@HOTMAIL.com
 */

#endregion

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Diagnostics.CodeAnalysis;

[module: SuppressMessage("Microsoft.Performance", "CA1815:OverrideEqualsAndOperatorEqualsOnValueTypes", Scope = "type", Target = "isr.Timing.CS.Multimedia.TimerCaps")]
[module: SuppressMessage("Microsoft.Usage", "CA2231:OverloadOperatorEqualsOnOverridingValueTypeEquals", Scope = "member", Target = "isr.Timing.CS.Multimedia.TimerCaps.#Equals(System.Object)")]
[module: SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable", Scope = "type", Target = "isr.Timing.CS.Multimedia.TimerStartException")]
[module: SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors", Scope = "type", Target = "isr.Timing.CS.Multimedia.TimerStartException")]
[module: SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Scope = "member", Target = "isr.Timing.CS.Multimedia.Timer.#.cctor()", MessageId = "isr.Timing.CS.Multimedia.SafeNativeMethods.timeGetDevCaps(isr.Timing.CS.Multimedia.TimerCaps@,System.Int32)")]
[module: SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", Scope = "member", Target = "isr.Timing.CS.Multimedia.Timer.#Finalize()", MessageId = "isr.Timing.CS.Multimedia.SafeNativeMethods.timeKillEvent(System.Int32)")]

namespace isr.Timing.CS.Multimedia

{
    /// <summary>
    /// Defines constants for the multimedia Timer's event types.
    /// </summary>
    public enum TimerMode
    {
        /// <summary>
        /// Timer event occurs once.
        /// </summary>
        OneShot,

        /// <summary>
        /// Timer event occurs periodically.
        /// </summary>
        Periodic
    };

    //TO_DO: Inherit from Fast Timer.

    /// <summary>
    /// Represents information about the multimedia Timer's capabilities.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct TimerCaps
    {
        /// <summary>
        /// Minimum supported period in milliseconds.
        /// </summary>
        public int PeriodMin { get; set; }

        /// <summary>
        /// Maximum supported period in milliseconds.
        /// </summary>
        public int PeriodMax { get; set; }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            return (PeriodMax == ((TimerCaps)obj).PeriodMax) && (PeriodMin == ((TimerCaps)obj).PeriodMin);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return PeriodMax.GetHashCode() ^ PeriodMin.GetHashCode();
        }
        
    }

    /// <summary>
    /// Represents the Windows multimedia timer.
    /// </summary>
    public sealed class Timer : IComponent
    {
    #region Timer Members

    #region Delegates

        // Represents methods that raise events.
        private delegate void EventRaiser(EventArgs e);

    #endregion

    #region Fields

        // Timer identifier.
        private int _timerID;

        // Timer mode.
        private volatile TimerMode _mode;

        // Period between timer events in milliseconds.
        private volatile int _period;

        // Timer resolution in milliseconds.
        private volatile int _resolution;        

        // Called by Windows when a timer periodic event occurs.
        private SafeNativeMethods.TimeProc _timeProcPeriodic;

        // Called by Windows when a timer one shot event occurs.
        private SafeNativeMethods.TimeProc _timeProcOneShot;

        // Represents the method that raises the Tick event.
        private EventRaiser _tickRaiser;

        // Indicates whether or not the timer is running.
        private bool _running ;

        // Indicates whether or not the timer has been disposed.
        private volatile bool _disposed ;

        // The I SynchronizeInvoke object to use for marshaling events.
        private ISynchronizeInvoke _synchronizingObject ;

        // For implementing I Component.
        private ISite _site ;

        // Multimedia timer capabilities.
        private static TimerCaps _capabilities;

    #endregion

    #region Events

        /// <summary>
        /// Occurs when the Timer has started;
        /// </summary>
        public event EventHandler Started;

        /// <summary>
        /// Occurs when the Timer has stopped;
        /// </summary>
        public event EventHandler Stopped;

        /// <summary>
        /// Occurs when the time period has elapsed.
        /// </summary>
        public event EventHandler Tick;

    #endregion

    #region Construction

        /// <summary>
        /// Initialize class.
        /// </summary>
        static Timer()
        {
            // Get multimedia timer capabilities.
            SafeNativeMethods.timeGetDevCaps(ref _capabilities, Marshal.SizeOf(_capabilities));
        }

        /// <summary>
        /// Initializes a new instance of the Timer class with the specified I Container.
        /// </summary>
        /// <param name="container">
        /// The I Container to which the Timer will add itself.
        /// </param>
        public Timer(IContainer container)
        {
            ///
            /// Required for Windows.Forms Class Composition Designer support
            ///
            if (container != null) 
            {
                container.Add(this);
            }                      

            Initialize();
        }

        /// <summary>
        /// Initializes a new instance of the Timer class.
        /// </summary>
        public Timer()
        {
            Initialize();
        }

        ~Timer()
        {
            if(IsRunning)
            {
                // Stop and destroy timer.
                // timeKillEvent(_timerID);
                SafeNativeMethods.timeKillEvent(_timerID);
            }
        }

        // Initialize timer with default values.
        private void Initialize()
        {
            this._mode = TimerMode.Periodic;
            this._period = Capabilities.PeriodMin;
            this._resolution = 1;

            // _running = false;

            _timeProcPeriodic = new SafeNativeMethods.TimeProc(TimerPeriodicEventCallback);
            _timeProcOneShot = new SafeNativeMethods.TimeProc(TimerOneShotEventCallback);
            _tickRaiser = new EventRaiser(OnTick);
        }

    #endregion

    #region Methods

        /// <summary>
        /// Starts the timer.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// The timer has already been disposed.
        /// </exception>
        /// <exception cref="TimerStartException">
        /// The timer failed to start.
        /// </exception>
        public void Start()
        {
            #region Require

            if(_disposed)
            {
                throw new ObjectDisposedException("Timer");
            }

            #endregion

            #region Guard

            if(IsRunning)
            {
                return;
            }

            #endregion

            // If the periodic event callback should be used.
            if(Mode == TimerMode.Periodic)
            {
                // Create and start timer.
                _timerID = SafeNativeMethods.timeSetEvent(Period, Resolution, _timeProcPeriodic, 0, (int)Mode);
            }
            // Else the one shot event callback should be used.
            else
            {
                // Create and start timer.
                _timerID = SafeNativeMethods.timeSetEvent(Period, Resolution, _timeProcOneShot, 0, (int)Mode);
            }

            // If the timer was created successfully.
            if(_timerID != 0)
            {
                _running = true;

                if(SynchronizingObject != null && SynchronizingObject.InvokeRequired)
                {
                    SynchronizingObject.BeginInvoke(
                        new EventRaiser(OnStarted), 
                        new object[] { EventArgs.Empty });
                }
                else
                {
                    OnStarted(EventArgs.Empty);
                }                
            }
            else
            {
                throw new TimerStartException("Unable to start multimedia Timer.");
            }
        }

        /// <summary>
        /// Stops timer.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// If the timer has already been disposed.
        /// </exception>
        public void Stop()
        {
            #region Require

            if(_disposed)
            {
                throw new ObjectDisposedException("Timer");
            }

            #endregion

            #region Guard

            if(!_running)
            {
                return;
            }

            #endregion

            // Stop and destroy timer.
            int result = SafeNativeMethods.timeKillEvent(_timerID);

            Debug.Assert(result == SafeNativeMethods.TIMERR_NOERROR);

            _running = false;

            if(SynchronizingObject != null && SynchronizingObject.InvokeRequired)
            {
                SynchronizingObject.BeginInvoke(
                    new EventRaiser(OnStopped), 
                    new object[] { EventArgs.Empty });
            }
            else
            {
                OnStopped(EventArgs.Empty);
            }
        }        

        #region Callbacks

        // Callback method called by the Win32 multimedia timer when a timer
        // periodic event occurs.
        private void TimerPeriodicEventCallback(int id, int msg, int user, int param1, int param2)
        {
            if(_synchronizingObject != null)
            {
                _synchronizingObject.BeginInvoke(_tickRaiser, new object[] { EventArgs.Empty });
            }
            else
            {
                OnTick(EventArgs.Empty);
            }
        }

        // Callback method called by the Win32 multimedia timer when a timer
        // one shot event occurs.
        private void TimerOneShotEventCallback(int id, int msg, int user, int param1, int param2)
        {
            if(_synchronizingObject != null)
            {
                _synchronizingObject.BeginInvoke(_tickRaiser, new object[] { EventArgs.Empty });
                Stop();
            }
            else
            {
                OnTick(EventArgs.Empty);
                Stop();
            }
        }

        #endregion

        #region Event Raiser Methods

        // Raises the Disposed event.
        private void OnDisposed(EventArgs e)
        {
            EventHandler handler = Disposed;

            if(handler != null)
            {
                handler(this, e);
            }
        }

        // Raises the Started event.
        private void OnStarted(EventArgs e)
        {
            EventHandler handler = Started;

            if(handler != null)
            {
                handler(this, e);
            }
        }

        // Raises the Stopped event.
        private void OnStopped(EventArgs e)
        {
            EventHandler handler = Stopped;

            if(handler != null)
            {
                handler(this, e);
            }
        }

        // Raises the Tick event.
        private void OnTick(EventArgs e)
        {
            EventHandler handler = Tick;

            if(handler != null)
            {
                handler(this, e);
            }
        }

    #endregion        

    #endregion

    #region Properties

        /// <summary>
        /// Gets or sets the object used to marshal event-handler calls.
        /// </summary>
        public ISynchronizeInvoke SynchronizingObject
        {
            get
            {
                #region Require

                if(_disposed)
                {
                    throw new ObjectDisposedException("Timer");
                }

                #endregion

                return _synchronizingObject;
            }
            set
            {
                #region Require

                if(_disposed)
                {
                    throw new ObjectDisposedException("Timer");
                }

                #endregion

                _synchronizingObject = value;
            }
        }

        /// <summary>
        /// Gets or sets the time between Tick events.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// If the timer has already been disposed.
        /// </exception>   
        public int Period
        {
            get
            {
                #region Require

                if(_disposed)
                {
                    throw new ObjectDisposedException("Timer");
                }

                #endregion

                return _period;
            }
            set
            {
                #region Require

                if(_disposed)
                {
                    throw new ObjectDisposedException("Timer");
                }
                else if(value < Capabilities.PeriodMin || value > Capabilities.PeriodMax)
                {
                    throw new ArgumentOutOfRangeException("value", value,
                        "Multimedia Timer period out of range.");
                }

                #endregion

                _period = value;

                if(IsRunning)
                {
                    Stop();
                    Start();
                }
            }
        }

        /// <summary>
        /// Gets or sets the timer resolution.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// If the timer has already been disposed.
        /// </exception>        
        /// <remarks>
        /// The resolution is in milliseconds. The resolution increases 
        /// with smaller values; a resolution of 0 indicates periodic events 
        /// should occur with the greatest possible accuracy. To reduce system 
        /// overhead, however, you should use the maximum value appropriate 
        /// for your application.
        /// </remarks>
        public int Resolution
        {
            get
            {
                #region Require

                if(_disposed)
                {
                    throw new ObjectDisposedException("Timer");
                }

                #endregion

                return _resolution;
            }
            set
            {
                #region Require

                if(_disposed)
                {
                    throw new ObjectDisposedException("Timer");
                }
                else if(value < 0)
                {
                    throw new ArgumentOutOfRangeException("value", value,
                        "Multimedia timer resolution out of range.");
                }

                #endregion

                _resolution = value;

                if(IsRunning)
                {
                    Stop();
                    Start();
                }
            }
        }

        /// <summary>
        /// Gets the timer mode.
        /// </summary>
        /// <exception cref="ObjectDisposedException">
        /// If the timer has already been disposed.
        /// </exception>
        public TimerMode Mode
        {
            get
            {
                #region Require

                if(_disposed)
                {
                    throw new ObjectDisposedException("Timer");
                }

                #endregion

                return _mode;
            }
            set
            {
                #region Require

                if(_disposed)
                {
                    throw new ObjectDisposedException("Timer");
                }

                #endregion
                
                _mode = value;

                if(IsRunning)
                {
                    Stop();
                    Start();
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Timer is running.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                return _running;
            }
        }

        /// <summary>
        /// Gets the timer capabilities.
        /// </summary>
        public static TimerCaps Capabilities
        {
            get
            {
                return _capabilities;
            }
        }

        #endregion

        #endregion

    #region IComponent Members

        public event System.EventHandler Disposed;

        public ISite Site
        {
            get
            {
                return _site;
            }
            set
            {
                _site = value;
            }
        }

    #endregion

    #region IDisposable Members

        /// <summary>
        /// Frees timer resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            #region Guard

                if (_disposed)
                {
                    return;
                }

            #endregion               

            if (disposing)
            {

                if (IsRunning)
                {
                    Stop();
                }

                _disposed = true;

                OnDisposed(EventArgs.Empty);
                
            }
        }
 
    #endregion       

    }

    /// <summary>
    /// The exception that is thrown when a timer fails to start.
    /// </summary>
    public class TimerStartException : System.Exception
    {
        /// <summary>
        /// Initializes a new instance of the TimerStartException class.
        /// </summary>
        /// <param name="message">
        /// The error message that explains the reason for the exception. 
        /// </param>
        public TimerStartException(string message) : base(message)
        {
        }
    }
}
